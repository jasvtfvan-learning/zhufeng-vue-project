实现路由的模块化，新建一个文件存放路由钩子函数<br />在src/router文件夹里新建hooks.js文件
```javascript
import store from '../store';
import * as types from '../store/action-types'
// 登录权限校验
const loginPermission = async function(to, from, next) {
  //console.log(this)//绑在router上
    let r = await store.dispatch(`user/${types.USER_VALIDATE}`);
   console.log(r)
    next();
}
export default {
    loginPermission,
}
```
注册hooks文件在src/router/index.js文件中
```javascript
import Vue from 'vue'
import VueRouter from 'vue-router'
import hooks from './hooks';
Vue.use(VueRouter)
//...
// 入口文件
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});
Object.values(hooks).forEach(hook=>{
  router.beforeEach(hook.bind(router)); // 将this绑定成router
});

export default router
```
退出登录的实现<br />src/components/PageHeader.vue
```javascript
//...
            <template slot="title">{{userInfo.username}}</template>
            <el-menu-item @click="$router.push('/manager')">管理后台</el-menu-item>
            <el-menu-item index="logout" @click="logout">退出登录</el-menu-item>
          </el-submenu>
        </el-menu>
      </div>
    </el-col>
  </el-row>
</template>
<script>
import { createNamespacedHelpers } from "vuex";
let { mapState,mapActions} = createNamespacedHelpers("user");
import * as types from '../store/action-types'
export default {
  computed: {
    ...mapState(["hasPermission", "userInfo"])
  },
  methods:{
    ...mapActions([types.USER_LOGOUT]),
    logout(){
      this[types.USER_LOGOUT]()
    } 
  }
};
</script>
```
在src/store/action-types.js文件中写一个退出登录方法
```javascript
//...
//验证退出登录
export const USER_LOGOUT = 'USER_LOGOUT';
```
在src/store/modules/user.js文件添加上
```javascript
import * as user from '@/api/user';
import * as types from '../action-types';
import { setLocal, getLocal } from '@/utils/local';
import router from '@/router'
export default {
    state: {
//...
    },
    mutations: {
//...
    },
    actions: {
//...
        async [types.USER_LOGOUT]({dispatch}){
            dispatch(types.SET_USER,{payload:{},permission:false})
        },
    }
}
```